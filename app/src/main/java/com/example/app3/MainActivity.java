package com.example.app3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {
    // currency and percent formatter objects
    private static final NumberFormat currencyFormat =
            NumberFormat.getCurrencyInstance();

    private double interestRate = 0.0;
    private double loan = 0.0;
    private double length5 = 0.0;
    private double length10 = 0.0;
    private double length15 = 0.0;
    private double length20 = 0.0;
    private double length25 = 0.0;
    private double length30 = 0.0;
    private TextView TextView05;
    private TextView TextView10;
    private TextView TextView15;
    private TextView TextView20;
    private TextView TextView25;
    private TextView TextView30;
    private TextView EditTextLoan;
    private TextView EditTextInterest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditTextLoan = (TextView) findViewById(R.id.editTextLoan);
        EditTextInterest = (TextView) findViewById(R.id.editTextInterest);
        TextView05 = (TextView) findViewById(R.id.textView05);
        TextView10 = (TextView) findViewById(R.id.textView10);
        TextView15 = (TextView) findViewById(R.id.textView15);
        TextView20 = (TextView) findViewById(R.id.textView20);
        TextView25 = (TextView) findViewById(R.id.textView25);
        TextView30 = (TextView) findViewById(R.id.textView30);
        TextView05.setText(currencyFormat.format(0));
        TextView10.setText(currencyFormat.format(0));
        TextView15.setText(currencyFormat.format(0));
        TextView20.setText(currencyFormat.format(0));
        TextView25.setText(currencyFormat.format(0));
        TextView30.setText(currencyFormat.format(0));
    }

    public void buttPress(View view) {

        if(!(EditTextInterest.getText().toString().isEmpty()) && !(EditTextLoan.getText().toString().isEmpty())) {
            interestRate = (Double.parseDouble(EditTextInterest.getText().toString())) / (12 * 100);
            loan = Double.parseDouble(EditTextLoan.getText().toString());

            length5 = (loan * interestRate * Math.pow(1 + interestRate, 60)) / (Math.pow(1 + interestRate, 60) - 1);
            length10 = (loan * interestRate * Math.pow(1 + interestRate, 120)) / (Math.pow(1 + interestRate, 120) - 1);
            length15 = (loan * interestRate * Math.pow(1 + interestRate, 180)) / (Math.pow(1 + interestRate, 180) - 1);
            length20 = (loan * interestRate * Math.pow(1 + interestRate, 240)) / (Math.pow(1 + interestRate, 240) - 1);
            length25 = (loan * interestRate * Math.pow(1 + interestRate, 300)) / (Math.pow(1 + interestRate, 300) - 1);
            length30 = (loan * interestRate * Math.pow(1 + interestRate, 360)) / (Math.pow(1 + interestRate, 360) - 1);

            TextView05.setText(currencyFormat.format(length5));
            TextView10.setText(currencyFormat.format(length10));
            TextView15.setText(currencyFormat.format(length15));
            TextView20.setText(currencyFormat.format(length20));
            TextView25.setText(currencyFormat.format(length25));
            TextView30.setText(currencyFormat.format(length30));
        }
    }
}